﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.Concurrent;

namespace LinkableGP.Agents
{
    public class PopulationBuilder : BlockingCollection<Chromosome>
    {
        public PopulationBuilder(int max)
            : base(max)
        {

        }

        public PopulationBuilder(int max, IEnumerable<Chromosome> seeds)
            :base(max)
        {
            foreach (Chromosome chrom in seeds)
            {
                if (!TryAdd(chrom))
                {
                    break;
                }
            }
        }

        public Population ToPopulation()
        {
            this.CompleteAdding();
            return new Population(this.ToArray());
        }

        public Population ToNonCompletingPopulation()
        {
            return new Population(this.ToArray());
        }
    }
}
