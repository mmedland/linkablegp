﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LinkableGP.Interfaces;
using LinkableGP.CodeGeneration;
using LinkableGP.Architecture;
using System.Runtime.Serialization;

namespace LinkableGP.Agents
{
    [Serializable]
    public class Chromosome : ICloneable, IProgramable, ISerializable, IComparable<Chromosome>, IComparable
    {
        public IFitness Fitness
        {
            get;
            set;
        }

        public INode Root
        {
            get;
            set;
        }

        public int MaxDepth
        {
            get;
            internal set;
        }

        public int CurrentDepth
        {
            get { return Root.Height; }
        }

        public Chromosome(int maxDepth)
        {
            MaxDepth = maxDepth;
        }

        protected Chromosome(SerializationInfo info, StreamingContext context)
        {
            Fitness = (IFitness)info.GetValue("fitness", typeof(IFitness));
            MaxDepth = info.GetInt32("depth");
            Root = (INode)info.GetValue("root", typeof(INode));
        }

        public void Simplify<T>(LanguagePool<T> pool) where T : new()
        {
            Constant c = SimplifyRecursively(Root, pool);
            if (c != null) Root = c;
        }

        private Constant SimplifyRecursively<T>(INode node, LanguagePool<T> pool) where T : new()
        {
            if (node is Constant)
            {
                return node as Constant;
            }
            else if (node is Variable)
            {
                return null;
            }
            else if (node is Function)
            {
                Function func = node as Function;
                bool simplifiable = true;
                for (int i = 0; i < func.Arguments.Length; i++)
                {
                    Constant c = SimplifyRecursively(func.Arguments[i], pool);
                    if (c != null)
                    {
                        func.Arguments[i] = c;
                    }
                    else
                    {
                        simplifiable = false;
                    }
                }
                if (simplifiable == true)
                {
                    object obj = node.Evaluate(pool.GetLanguageInstance());
                    return pool.MakeConstant(node.ValueType, obj);
                }
                else
                {
                    return null;
                }
            }
            else
            {
                return null;
            }
        }

        public object Evaluate(object languageInstance)
        {
            return Root.Evaluate(languageInstance);
        }

        public static bool operator >(Chromosome c1, Chromosome c2)
        {
            return c1.Fitness.CompareTo(c2.Fitness) > 0;
        }

        public static bool operator <(Chromosome c1, Chromosome c2)
        {
            return c1.Fitness.CompareTo(c2.Fitness) < 0;
        }

        public static bool operator >=(Chromosome c1, Chromosome c2)
        {
            return c1.Fitness.CompareTo(c2.Fitness) >= 0;
        }

        public static bool operator <=(Chromosome c1, Chromosome c2)
        {
            return c1.Fitness.CompareTo(c2.Fitness) <= 0;
        }

        public object Clone()
        {
            Chromosome c = new Chromosome(this.MaxDepth);
            c.Root = (INode)this.Root.Clone();
            c.Fitness = (IFitness)this.Fitness.Clone();
            return c;
        }

        public ICompilable GetProgramRoot()
        {
            return Root;
        }

        public Type GetReturnType()
        {
            return Root.ValueType;
        }


        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("fitness", Fitness);
            info.AddValue("depth", MaxDepth);
            info.AddValue("root", Root);
        }

        public int CompareTo(Chromosome other)
        {
            return this.Fitness.CompareTo(other.Fitness);
        }

        public int CompareTo(object obj)
        {
            return CompareTo(obj as Chromosome);
        }

        public override string ToString()
        {
            return Fitness.ToString();
        }
    }
}
