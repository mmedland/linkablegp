﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.Concurrent;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

namespace LinkableGP.Agents
{
    [Serializable]
    public class Population : IEnumerable<Chromosome>, ISerializable
    {
        Chromosome[] pop;
        Random rand;

        public int Size
        {
            get { return pop.Length; }
        }

        public Chromosome this[int index]
        {
            get { return pop[index]; }
        }
        
        internal Population(Chromosome[] population)
        {
            pop = population;
            rand = new Random();
        }

        protected Population(SerializationInfo info, StreamingContext context)
        {
            pop = new Chromosome[info.GetInt32("size")];
            for (int i = 0; i < Size; i++)
            {
                pop[i] = (Chromosome)info.GetValue("chrom" + i.ToString(), typeof(Chromosome));
            }
            rand = new Random();
        }

        public Chromosome GetRandom()
        {
            return pop[rand.Next(pop.Length)];
        }

        public Chromosome[] GetNRandom(int n)
        {
            Chromosome[] subset = new Chromosome[n];
            for (int i = 0; i < n; i++)
            {
                subset[i] = GetRandom();
            }
            return subset;
        }

        public IEnumerator<Chromosome> GetEnumerator()
        {
            foreach (Chromosome c in pop)
            {
                yield return c;
            }
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return pop.GetEnumerator();
        }

        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("size", Size);
            for (int i = 0; i < Size; i++)
            {
                info.AddValue("chrom" + i.ToString(), pop[i]);
            }
        }

        public static void Save(string filepath, bool append, params Chromosome[] chromosomes)
        {
            BinaryFormatter bf = new BinaryFormatter();

            PopulationBuilder builder;
            if (append && File.Exists(filepath))
            {
                using (Stream stream = File.Open(filepath, FileMode.Open))
                {
                    Population pop = (Population)bf.Deserialize(stream);
                    builder = new PopulationBuilder(pop.Size + chromosomes.Length);
                    foreach (Chromosome chrom in pop)
                    {
                        builder.Add(chrom);
                    }
                }
            }
            else
            {
                builder = new PopulationBuilder(chromosomes.Length);
            }
            foreach (Chromosome chrom in chromosomes)
            {
                builder.Add(chrom);
            }
            Population p = builder.ToPopulation();
            using (Stream pstream = File.Create(filepath))
            {
                bf.Serialize(pstream, p);
            }
        }

        public static Population Load(string filepath)
        {
            BinaryFormatter bf = new BinaryFormatter();
            Stream stream = File.Open(filepath, FileMode.Open);
            return (Population)bf.Deserialize(stream);
        }

        public IEnumerable<Chromosome> GetBestN(int n)
        {
            Chromosome[] sorted = (Chromosome[])pop.Clone();
            Array.Sort(sorted);
            Chromosome[] best = new Chromosome[n];
            for (int i = 0; i < n; i++)
            {
                best[i] = (Chromosome)sorted[sorted.Length - 1 - i].Clone();
            }
            return best;
        }
    }
}
