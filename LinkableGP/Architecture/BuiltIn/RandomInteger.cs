﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LinkableGP.Architecture.BuiltIn
{
    /// <summary>
    /// Ephemeral Constant Generator for ConstantIntegers.
    /// </summary>
    public class RandomInteger : IRandom
    {
        /// <summary>
        /// Lowest value
        /// </summary>
        private int _lowest;

        /// <summary>
        /// Highest value
        /// </summary>
        private int _highest;

        /// <summary>
        /// Random generator for this instance
        /// </summary>
        private Random _generator;

        /// <summary>
        /// The type of this value. Always returns typeof(double)
        /// </summary>
        public Type ValueType
        {
            get { return typeof(int); }
        }

        /// <summary>
        /// Creates a generator bounded by [0, int.MaxValue)
        /// </summary>
        public RandomInteger()
            : this(0, int.MaxValue)
        {
        }

        /// <summary>
        /// Creates a generator where the lowest and highest values are given.
        /// </summary>
        /// <param name="lowest">the lower bound of the generator (inclusive).</param>
        /// <param name="highest">the upper bound of the generator (exclusive).</param>
        public RandomInteger(int lowest, int highest)
        {
            _generator = new Random();
            _lowest = lowest;
            _highest = highest;
        }

        /// <summary>
        /// Creates a new ConstantInteger
        /// </summary>
        /// <returns></returns>
        public Constant GetNextConstant()
        {
            int value = _generator.Next(_lowest, _highest);
            return new ConstantInteger(value);
        }
    }
}
