﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LinkableGP.Architecture.BuiltIn
{
    /// <summary>
    /// Ephemeral Constant Generator for ConstantDoubles.
    /// </summary>
    public class RandomDouble : IRandom
    {
        /// <summary>
        /// Lowest value
        /// </summary>
        private double _lowest;

        /// <summary>
        /// Highest value
        /// </summary>
        private double _highest;

        /// <summary>
        /// Random generator for this instance
        /// </summary>
        private Random _generator;

        /// <summary>
        /// The type of this value. Always returns typeof(double)
        /// </summary>
        public Type ValueType
        {
            get { return typeof(double); }
        }

        /// <summary>
        /// Creates a generator bounded by [0, 1)
        /// </summary>
        public RandomDouble()
            :this(0, 1)
        {
        }

        /// <summary>
        /// Creates a generator where the lowest and highest values are given.
        /// </summary>
        /// <param name="lowest">the lower bound of the generator (inclusive).</param>
        /// <param name="highest">the upper bound of the generator (exclusive).</param>
        /// <remarks>
        /// WARNING: do not set both lowest and highest as double.MinValue and double.MaxValue, respectively,
        /// as this will cause a wrap-around in the double.
        /// highest - lowest should be less than or equal to double.MaxValue
        /// </remarks>
        public RandomDouble(double lowest, double highest)
        {
            _generator = new Random();
            _lowest = lowest;
            _highest = highest;
        }

        /// <summary>
        /// Creates a new ConstantDouble
        /// </summary>
        /// <returns></returns>
        public Constant GetNextConstant()
        {
            double value = _generator.NextDouble() * (_highest - _lowest) + _lowest;
            return new ConstantDouble(value);
        }
    }
}
