﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LinkableGP.Architecture;
using System.CodeDom;
using System.Runtime.Serialization;

namespace LinkableGP.Architecture.BuiltIn
{
    /// <summary>
    /// This class implements Constants for values of type double.
    /// </summary>
    [Serializable]
    public sealed class ConstantDouble : Constant
    {
        /// <summary>
        /// The value of this constant
        /// </summary>
        private double _value;

        /// <summary>
        /// The value of this Constant
        /// </summary>
        public override object Value
        {
            get { return _value; }
            set { _value = (double)value; }
        }

        /// <summary>
        /// Constructs a ConstantDouble using the serialization info
        /// </summary>
        /// <param name="info"></param>
        /// <param name="context"></param>
        public ConstantDouble(SerializationInfo info, StreamingContext context)
        {
            _value = info.GetDouble("value");
            _type = typeof(double);
        }

        /// <summary>
        /// Instantiates an emphemeral constant of type double.
        /// </summary>
        /// <param name="value">the value to be associated.</param>
        public ConstantDouble(double value)
        {
            _value = value;
            _type = typeof(double);
        }

        /// <summary>
        /// Gerenates a CodeDOM Primitive Expression for the value associated to this constant.
        /// </summary>
        /// <returns>A Code Expression of type CodePrimitiveExpression</returns>
        public override CodeExpression ToCodeExpression()
        {
            return new CodePrimitiveExpression(_value);
        }

        /// <summary>
        /// Copies the value to a new instance of ConstantDouble
        /// </summary>
        /// <returns>A new ConstantDouble</returns>
        public override object Clone()
        {
            return new ConstantDouble(_value);
        }

        /// <summary>
        /// Serializes a ConstantDouble with its value
        /// </summary>
        /// <param name="info"></param>
        /// <param name="context"></param>
        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("value", _value);
        }
    }
}
