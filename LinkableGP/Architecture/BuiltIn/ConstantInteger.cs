﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.CodeDom;
using System.Runtime.Serialization;

namespace LinkableGP.Architecture.BuiltIn
{
    /// <summary>
    /// This class implements Constants for values of type int.
    /// </summary>
    [Serializable]
    public sealed class ConstantInteger : Constant
    {
        /// <summary>
        /// The value of this constant
        /// </summary>
        private int _value;

        /// <summary>
        /// The value of this constant
        /// </summary>
        public override object Value
        {
            get { return _value; }
            set { _value = (int)value; }
        }

        /// <summary>
        /// Constructs a ConstantInteger using the serialization info
        /// </summary>
        /// <param name="info"></param>
        /// <param name="context"></param>
        public ConstantInteger(SerializationInfo info, StreamingContext context)
        {
            _value = info.GetInt32("value");
            _type = typeof(Int32);
        }

        /// <summary>
        /// Instantiates an emphemeral constant of type int.
        /// </summary>
        /// <param name="value">the value to be associated.</param>
        public ConstantInteger(int value)
        {
            _value = value;
            _type = typeof(Int32);
        }

        /// <summary>
        /// Gerenates a CodeDOM Primitive Expression for the value associated to this constant.
        /// </summary>
        /// <returns>A Code Expression of type CodePrimitiveExpression</returns>
        public override CodeExpression ToCodeExpression()
        {
            return new CodePrimitiveExpression(_value);
        }

        /// <summary>
        /// Copies the value to a new instance of ConstantDouble
        /// </summary>
        /// <returns>A new ConstantDouble</returns>
        public override object Clone()
        {
            return new ConstantInteger(_value);
        }

        /// <summary>
        /// Serializes the value of the ConstantInteger
        /// </summary>
        /// <param name="info"></param>
        /// <param name="context"></param>
        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("value", _value);
        }
    }
}
