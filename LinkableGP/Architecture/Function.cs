﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using System.Runtime.Serialization;
using System.CodeDom;
using LinkableGP.CodeGeneration;

namespace LinkableGP.Architecture
{
    /// <summary>
    /// This class represents a function node in the program tree. It is a non-leaf node
    /// and recursively invokes its argument nodes for evaluation.
    /// </summary>
    [Serializable]
    public class Function : INode
    {
        /// <summary>
        /// The MethodInfo that represents this function
        /// </summary>
        private MethodInfo _function;

        /// <summary>
        /// The return type of this function.
        /// </summary>
        public Type ValueType
        {
            get { return _function.ReturnType; }
        }

        /// <summary>
        /// The maximum subtree height + 1
        /// </summary>
        public int Height
        {
            get { return Arguments.Max(x=>x.Height) + 1; }
        }

        /// <summary>
        /// The ValueType required for each Argument in Arguments of this instance
        /// </summary>
        public Type[] ArgumentTypes
        {
            get;
            private set;
        }

        /// <summary>
        /// The arguments of this function
        /// </summary>
        public INode[] Arguments
        {
            get;
            private set;
        }

        /// <summary>
        /// Constructs a function using the serialization information.
        /// </summary>
        /// <param name="info"></param>
        /// <param name="context"></param>
        public Function(SerializationInfo info, StreamingContext context)
        {
            _function = (MethodInfo)info.GetValue("function", typeof(MethodInfo));
            int argc = _function.GetParameters().Length;
            ArgumentTypes = new Type[argc];
            Arguments = new INode[argc];
            for (int i = 0; i < argc; i++)
            {
                Type argType = (Type)info.GetValue("type" + i.ToString(), typeof(Type));
                Arguments[i] = (INode)info.GetValue("argument" + i.ToString(), argType);
                ArgumentTypes[i] = Arguments[i].ValueType;
            }
        }

        /// <summary>
        /// Constructs a function with the given method info.
        /// </summary>
        /// <param name="method">the method info that describes this function</param>
        public Function(MethodInfo method)
        {
            _function = method;
            ParameterInfo[] parameters = _function.GetParameters();
            ArgumentTypes = new Type[parameters.Length];
            Arguments = new INode[parameters.Length];
            for (int i = 0; i < ArgumentTypes.Length; i++)
            {
                ArgumentTypes[i] = parameters[i].ParameterType;
            }
        }

        /// <summary>
        /// Evaluates the function by invoking the evaluation of its Arguments. When evaluation is conducted
        /// no Argument from the Arguments property of this function can be null. An expection is thrown if this
        /// is the case. Similarly, an acception may be thrown if the language instance is null.
        /// </summary>
        /// <param name="languageInstance">The instance of the language that contains this function</param>
        /// <returns>The result of the evaluation</returns>
        public object Evaluate(object languageInstance)
        {
            object[] inputs = new object[Arguments.Length];
            for (int i = 0; i < Arguments.Length; i++)
            {
                inputs[i] = Arguments[i].Evaluate(languageInstance);
            }
            return _function.Invoke(languageInstance, inputs);
        }

        /// <summary>
        /// Constructs a CodeMethodReferenceExpression for this function. Only the method name is set, instance must be set by caller.
        /// </summary>
        /// <returns>A CodeMethodReferenceExpression</returns>
        public CodeExpression ToCodeExpression()
        {
            CodeMethodReferenceExpression expr = new CodeMethodReferenceExpression();
            expr.MethodName = _function.Name;
            return expr;
        }

        /// <summary>
        /// Copies the MethodInfo of this function into a new Function
        /// </summary>
        /// <returns>A Copy of this Function</returns>
        public object Clone()
        {
            MethodInfo function = _function.DeclaringType.GetMethod(_function.Name, ArgumentTypes);
            Function copy = new Function(function);
            for (int i = 0; i < Arguments.Length; i++)
            {
                copy.Arguments[i] = (INode)this.Arguments[i].Clone();
            }
            return copy;
        }

        /// <summary>
        /// Serializes this function.
        /// </summary>
        /// <param name="info"></param>
        /// <param name="context"></param>
        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("function", _function, typeof(MethodInfo));
            for (int i = 0; i < Arguments.Length; i++)
            {
                info.AddValue("type" + i.ToString(), Arguments[i].GetType());
                info.AddValue("argument" + i.ToString(), Arguments[i]);
            }
        }

        public void SetArgument(int i, INode node)
        {
            if (node.ValueType != ArgumentTypes[i]) throw new TypeMismatchException();
            else Arguments[i] = node;
        }

        /// <summary>
        /// gets the Collection of compilable arguments
        /// </summary>
        /// <returns>the list of compilable arguments</returns>
        public ICompilable[] GetArguments()
        {
            ICompilable[] args = new ICompilable[Arguments.Length];
            for (int i = 0; i < Arguments.Length; i++)
            {
                args[i] = Arguments[i] as ICompilable;
            }
            return args;
        }


        public bool HasArguments()
        {
            return true;
        }
    }
}
