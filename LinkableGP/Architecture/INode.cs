﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace LinkableGP.Architecture
{
    /// <summary>
    /// This interface represents a Node for a program tree
    /// </summary>
    public interface INode : IEvaluable, LinkableGP.CodeGeneration.ICompilable, ICloneable, ISerializable
    {
        /// <summary>
        /// The value type returned by this type
        /// </summary>
        Type ValueType
        {
            get;
        }

        /// <summary>
        /// The maximum height of the subtree of this node
        /// </summary>
        int Height
        {
            get;
        }
    }
}
