﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;

namespace LinkableGP.Architecture
{
    public class LanguagePool<T>
        where T : new()
    {
        /// <summary>
        /// Random Number Generator used for Node Selection
        /// </summary>
        private Random rand;

        /// <summary>
        /// Ephemeral Constant Generators Collection
        /// </summary>
        private IRandom[] _rands;

        /// <summary>
        /// List of fields in the language
        /// </summary>
        private IList<FieldInfo> fields;

        /// <summary>
        /// List of methods in the language
        /// </summary>
        private IList<MethodInfo> methods;

        /// <summary>
        /// the root type return by program trees
        /// </summary>
        public Type RootType
        {
            get;
            private set;
        }


        /// <summary>
        /// Constructs a Language Pool using the generic type. The language consists of: the non-static fields as
        /// terminal variables; the non-static, public, non-void returning, parameter accepting methods not named Equals
        /// and the constants provided by the collection of randoms
        /// </summary>
        /// <param name="root">The root type expected for program trees</param>
        /// <param name="randoms">The random generators for ephemeral constants</param>
        public LanguagePool(Type root, params IRandom[] randoms)
        {
            fields = new List<FieldInfo>();
            methods = new List<MethodInfo>();
            rand = new Random();
            _rands = randoms;
            RootType = root;
            Type languageType = typeof(T);
            foreach (MemberInfo member in languageType.GetMembers())
            {
                switch (member.MemberType)
                {
                    case MemberTypes.Method:
                        MethodInfo method = member as MethodInfo;
                        if (method.IsPublic && !method.IsStatic
                            && method.GetParameters().Length > 0 && method.ReturnType != typeof(void)
                            && method.Name != "Equals")
                            methods.Add(method);
                        break;
                    case MemberTypes.Field:
                        FieldInfo field = member as FieldInfo;
                        if (!field.IsStatic)
                            fields.Add(field);
                        break;
                    default:
                        //all other member types are ignored.
                        break;
                }
            }
        }

        /// <summary>
        /// Gets a random function that returns the given type
        /// </summary>
        /// <param name="type">The desired return type</param>
        /// <returns>A random function that returns the given type.</returns>
        public Function GetFunctionOfType(Type type = null)
        {
            if (type == null) type = RootType;
            IList<Function> selected = new List<Function>();
            foreach (MethodInfo method in methods)
            {
                if (method.ReturnType == type)
                {
                    selected.Add(new Function(method));
                }
            }
            selected.Shuffle(rand);
            return selected.ElementAt(rand.Next(selected.Count));
        }

        /// <summary>
        /// Gets a random terminal(Variable or Constant) that is of the same type as given
        /// </summary>
        /// <param name="type">The desired terminal type</param>
        /// <returns>A random terminal of the given type</returns>
        public Terminal GetTerminalOfType(Type type = null)
        {
            if (type == null) type = RootType;
            IList<Terminal> selected = new List<Terminal>();
            foreach (FieldInfo field in fields)
            {
                if (field.FieldType == type)
                {
                    selected.Add(new Variable(field));
                }
            }
            foreach (IRandom generator in _rands)
            {
                if (generator.ValueType == type)
                {
                    selected.Add(generator.GetNextConstant());
                }
            }
            selected.Shuffle(rand);
            return selected.ElementAt(rand.Next(selected.Count));
        }

        /// <summary>
        /// Gets a random node of any Form(Function, Variable, or Constant) that is of the given type.
        /// I.E. returns the type (Functions), or is of the type (Variable or Constant).
        /// </summary>
        /// <param name="type">The desired type</param>
        /// <returns>A random node of the given type</returns>
        public INode GetNodeOfType(Type type = null)
        {
            if (type == null) type = RootType;
            IList<INode> selected = new List<INode>();
            foreach (MethodInfo method in methods)
            {
                if (method.ReturnType == type)
                {
                    selected.Add(new Function(method));
                }
            }
            foreach (FieldInfo field in fields)
            {
                if (field.FieldType == type)
                {
                    selected.Add(new Variable(field));
                }
            }
            foreach (IRandom generator in _rands)
            {
                if (generator.ValueType == type)
                {
                    selected.Add(generator.GetNextConstant());
                }
            }
            selected.Shuffle(rand);
            return selected.ElementAt(rand.Next(selected.Count));
        }

        /// <summary>
        /// provides a instance of the language type for which this language pool provides
        /// </summary>
        /// <returns></returns>
        internal object GetLanguageInstance()
        {
            return new T();
        }

        internal Constant MakeConstant(Type t, object value)
        {
            foreach (IRandom generator in _rands)
            {
                if (generator.ValueType == t)
                {
                    Constant c = generator.GetNextConstant();
                    c.Value = value;
                    return c;
                }
            }
            return null;
        }
    }

    /// <summary>
    /// Static class for extending list
    /// </summary>
    internal static class ListExtension
    {
        /// <summary>
        /// Shuffles the list.
        /// </summary>
        /// <typeparam name="T">The list elements</typeparam>
        /// <param name="list">The list</param>
        /// <param name="rand">a random generator(null values causes one to be constructed)</param>
        public static void Shuffle<T>(this IList<T> list, Random rand = null)
        {
            if (rand == null) rand = new Random();
            int n = list.Count;
            while (n > 1)
            {
                n--;
                int k = rand.Next(n + 1);
                T value = list[k];
                list[k] = list[n];
                list[n] = value;
            } 
        }
    }
}
