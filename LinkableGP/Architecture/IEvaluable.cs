﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LinkableGP.Architecture
{
    /// <summary>
    /// Interface that specifies the evaluation method of a node in the program tree.
    /// </summary>
    public interface IEvaluable
    {
        /// <summary>
        /// Called by Chromosome evaluation to evaluate the expression on a given language instance.
        /// </summary>
        /// <param name="languageInstance">the instance of the language</param>
        /// <returns>the result of the evaluation</returns>
        object Evaluate(object languageInstance);
    }
}
