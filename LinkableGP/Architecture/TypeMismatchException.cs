﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LinkableGP.Architecture
{
    /// <summary>
    /// Thrown when a type mismatch occurs with a node.
    /// </summary>
    class TypeMismatchException : Exception
    {
    }
}
