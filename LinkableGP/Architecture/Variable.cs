﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using System.CodeDom;
using System.Runtime.Serialization;

namespace LinkableGP.Architecture
{
    /// <summary>
    /// This class represents a terminal of variable type in the program expression tree.
    /// </summary>
    [Serializable]
    internal class Variable : Terminal
    {
        /// <summary>
        /// The field that defines the variable
        /// </summary>
        private FieldInfo _field;

        /// <summary>
        /// Constructs a variable using serialization information.
        /// </summary>
        /// <param name="info"></param>
        /// <param name="context"></param>
        protected Variable(SerializationInfo info, StreamingContext context)
        {
            _field = (FieldInfo)info.GetValue("field", typeof(FieldInfo));
            _type = _field.FieldType;
        }

        /// <summary>
        /// Constructs a variable using a field.
        /// </summary>
        /// <param name="field"></param>
        public Variable(FieldInfo field)
        {
            _type = field.FieldType;
            _field = field;
        }

        /// <summary>
        /// Constructs a CodeParameterDeclarationExpression that represents the type 
        /// and name of the variable.
        /// </summary>
        /// <returns>A CodeParameterDeclarationExpression</returns>
        public override CodeExpression ToCodeExpression()
        {
            return new CodeParameterDeclarationExpression(_type, _field.Name);
        }

        /// <summary>
        /// Copies the FieldInfo of this instance into a new instance of Variable
        /// </summary>
        /// <returns>A copy of this Variable</returns>
        public override object Clone()
        {
            FieldInfo field = _field.DeclaringType.GetField(_field.Name);
            return new Variable(field);
        }

        /// <summary>
        /// Returns the value of this variable given an instance of the language. 
        /// Will throw an expection if language is not of the correct type.
        /// </summary>
        /// <param name="languageInstance">An instance of the language where this variable is defined</param>
        /// <returns>The value of this variable from the language instance</returns>
        public override object Evaluate(object languageInstance)
        {
            return _field.GetValue(languageInstance);
        }

        /// <summary>
        /// Serializes the variable
        /// </summary>
        /// <param name="info"></param>
        /// <param name="context"></param>
        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("field", _field, typeof(FieldInfo));
        }
    }
}
