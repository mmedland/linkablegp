﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.CodeDom;

namespace LinkableGP.Architecture
{
    /// <summary>
    /// This class represents an emphemeral constant in the expression tree of a genetic program.
    /// </summary>
    [Serializable]
    public abstract class Constant : Terminal
    {
        /// <summary>
        /// The constant value of the instance. This value is
        /// used in the Evaluation call of the constant.
        /// </summary>
        public abstract object Value
        {
            get;
            set;
        }

        /// <summary>
        /// Evaluation sequence called during chromosome evaluation of its tree.
        /// </summary>
        /// <param name="languageInstance">Genetic Program Language Instance. This is ignored for Constants</param>
        /// <returns>the constant value of this instance</returns>
        public override object Evaluate(object languageInstance)
        {
            return Value;
        }

        
    }
}
