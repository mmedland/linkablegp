﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LinkableGP.Architecture
{
    /// <summary>
    /// This abstract class represents a terminal node. These are the leaf nodes
    /// of the program tree.
    /// </summary>
    public abstract class Terminal : INode
    {
        /// <summary>
        /// The type of the terminal.
        /// </summary>
        protected Type _type;

        /// <summary>
        /// The type of the terminal
        /// </summary>
        public Type ValueType
        {
            get { return _type; }
        }

        /// <summary>
        /// The height of the subtree for a terminal is always 0
        /// </summary>
        public int Height
        {
            get { return 0; }
        }

        /// <summary>
        /// A CodeDOM expression that represents this terminal
        /// </summary>
        /// <returns>A reference to the terminal</returns>
        public abstract System.CodeDom.CodeExpression ToCodeExpression();

        /// <summary>
        /// Implementations should clone the terminal
        /// </summary>
        /// <returns></returns>
        public abstract object Clone();

        /// <summary>
        /// Implementations should return the value of the terminal
        /// </summary>
        /// <param name="languageInstance">An instance of the language</param>
        /// <returns>the value of the terminal</returns>
        public abstract object Evaluate(object languageInstance);

        /// <summary>
        /// Serialization for terminal
        /// </summary>
        /// <param name="info"></param>
        /// <param name="context"></param>
        public abstract void GetObjectData(System.Runtime.Serialization.SerializationInfo info, 
            System.Runtime.Serialization.StreamingContext context);

        /// <summary>
        /// Since terminals by definition have no arguments this method always returns zero-lengthed array;
        /// </summary>
        /// <returns>zero-lengthed array</returns>
        public CodeGeneration.ICompilable[] GetArguments()
        {
            return new CodeGeneration.ICompilable[0];
        }


        public bool HasArguments()
        {
            return false;
        }
    }
}
