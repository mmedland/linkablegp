﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LinkableGP.Agents;

namespace LinkableGP.Interfaces
{
    /// <summary>
    /// This is the environment in which the chromosome will run and be evaluated.
    /// </summary>
    public interface IEnvironment
    {
        void Evaluate(Chromosome c, object languageInstance);
    }
}
