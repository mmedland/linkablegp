﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LinkableGP.Interfaces;
using LinkableGP.Architecture;
using LinkableGP.Agents;
using LinkableGP.Algorithms;
using System.Threading;

namespace LinkableGP
{
    public class LinkableGPTrainer
    {
        public delegate bool StoppingCriteriaDelegate(Population pop, int iteration);
    }

    public class LinkableGPTrainer<T, U> : LinkableGPTrainer where T : IEnvironment, new() where U : new()
    {

        private Population pop;
        private LanguagePool<U> languagePool;
        private GeneticOperations ops;
        private int maxDepth;

        public LinkableGPTrainer(LanguagePool<U> languagePool, int popSize, int initDepth, int maxDepth, GeneticOperations ops, Population seedpop = null, double propSeeded = 0.1)
        {
            this.languagePool = languagePool;
            this.ops = ops;
            this.maxDepth = maxDepth;
            PopulationBuilder builder;
            if (seedpop != null)
            {
                Chromosome[] seeds = seedpop.GetNRandom((int)(popSize * propSeeded));
                builder = new PopulationBuilder(popSize, seeds);
            }
            else
            {
                builder = new PopulationBuilder(popSize);
            }

            Semaphore semaphore = new Semaphore(0, Environment.ProcessorCount);
            
            for (int i = 0; i < Environment.ProcessorCount; i++)
            {
                new Thread(new ThreadStart(() =>
                    {
                        T envir = new T();
                        GeneticOperations tops;
                        lock (ops)
                        {
                            tops = (GeneticOperations)ops.Clone();
                        }
                        while (true)
                        {
                            Chromosome c = ops.BuildChromosome(languagePool, initDepth);
                            c.MaxDepth = maxDepth;
                            envir.Evaluate(c, languagePool.GetLanguageInstance());
                            if (!builder.TryAdd(c))
                            {
                                break;
                            }
                        }
                        semaphore.Release(1);
                    }
                )).Start();
            }

            for (int i = 0; i < Environment.ProcessorCount; i++)
            {
                semaphore.WaitOne();
            }
            pop = builder.ToPopulation();
        }

        public Chromosome Run(double crossrate, double mutrate, double elitismRate, LinkableGPTrainer.StoppingCriteriaDelegate isStop, bool keepGenerationBest = false)
        {
            GeneticAlgorithm ga = new GeneticAlgorithm(crossrate, mutrate, ops, maxDepth, elitismRate);
            Chromosome best = pop.Max();
            int gen = 0;
            while (!isStop(pop, gen))
            {
                pop = ga.Run<U,T>(pop, languagePool);
                if (keepGenerationBest)
                {
                    best = pop.Max();
                }
                else
                {
                    Chromosome elite = pop.Max();
                    if (elite >= best)
                    {
                        best = elite;
                    }
                }
                gen++;
            }
            return best;
        }
    }
}
