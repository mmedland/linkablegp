﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.CodeDom;
using LinkableGP.Architecture;
using System.CodeDom.Compiler;
using System.IO;
using System.Reflection;

namespace LinkableGP.CodeGeneration
{
    public class Programmer<T>
    {
        /// <summary>
        /// Delegate that models the builder
        /// </summary>
        /// <param name="program">The root node of the program tree</param>
        /// <returns>A CodeDom graph of the program</returns>
        private delegate void builderDelegate(ref CodeMemberMethod method, IProgramable program);

        private CodeDomProvider provider;
        private builderDelegate builder;

        /// <summary>
        /// The style of Programming to use.
        /// </summary>
        public enum ProgrammingStyle
        {
            InlineCode/*Each call is atomic*/, NestedCalls/*all the calls are nested beginning at the root*/
        }

        /// <summary>
        /// Constructs a programmer that uses the given programming style
        /// </summary>
        /// <param name="style">the style of programming the program tree</param>
        public Programmer(ProgrammingStyle style = ProgrammingStyle.NestedCalls)
        {
            provider = CodeDomProvider.CreateProvider("CSharp");

            switch (style)
            {
                case ProgrammingStyle.NestedCalls:
                    builder = new builderDelegate(NestedBuild);
                    break;

                case ProgrammingStyle.InlineCode:
                    builder = new builderDelegate(InlineBuild);
                    break;
            }
        }

        /// <summary>
        /// Builds the program tree.
        /// </summary>
        /// <param name="program">The root node of the compilable program tree</param>
        /// <returns></returns>
        public CodeCompileUnit Build(IProgramable program)
        {
            CodeCompileUnit unit = new CodeCompileUnit();

            //namespace and imports
            CodeNamespace progNamespace = new CodeNamespace("LinkableGPAGF");
            Type langType = typeof(T);
            progNamespace.Imports.Add(new CodeNamespaceImport(langType.Namespace));
            foreach (AssemblyName assembly in langType.Assembly.GetReferencedAssemblies())
            {
                if (!assembly.Name.Contains("mscorlib") && !assembly.Name.Contains("System.Core"))
                {
                    progNamespace.Imports.Add(new CodeNamespaceImport(assembly.Name));
                }
            }
            unit.Namespaces.Add(progNamespace);

            //new class
            CodeTypeDeclaration class1 = new CodeTypeDeclaration("AGFClass");
            class1.Attributes = MemberAttributes.Public;
            progNamespace.Types.Add(class1);

            CodeMemberField field = new CodeMemberField(langType, "lang");
            field.Attributes = MemberAttributes.Public;
            class1.Members.Add(field);
            CodeObjectCreateExpression fieldCreate = new CodeObjectCreateExpression(langType);
            CodeVariableReferenceExpression fieldRef = new CodeVariableReferenceExpression("lang");
            CodeAssignStatement fieldAssign = new CodeAssignStatement(fieldRef, fieldCreate);

            CodeConstructor ctr = new CodeConstructor();
            ctr.Attributes = MemberAttributes.Public;
            ctr.Statements.Add(fieldAssign);

            class1.Members.Add(ctr);

            //code the AGF
            CodeMemberMethod method = new CodeMemberMethod();
            method.Name = "AGF1";
            method.ReturnType = new CodeTypeReference(program.GetReturnType());
            method.Attributes = MemberAttributes.Public;
            builder(ref method, program);
            class1.Members.Add(method);

            return unit;
        }

        /// <summary>
        /// Generates the source to the cs filename provided
        /// </summary>
        /// <param name="code">the code to generate source for</param>
        /// <param name="filename">the file source</param>
        public void ToSource(CodeCompileUnit code, string filename)
        {
            string file;
            if (!filename.EndsWith(".cs")) file = filename + ".cs";
            else file = filename;
            StreamWriter sw = new StreamWriter(file);
            IndentedTextWriter tw = new IndentedTextWriter(sw);
            provider.GenerateCodeFromCompileUnit(code, tw, new CodeGeneratorOptions());
            tw.Close();
            sw.Close();
        }

        /// <summary>
        /// Compiles the given code to a dll of the same namespace as the first namespace in the compileunit
        /// </summary>
        /// <param name="code">the code to be compiled</param>
        /// <returns>the results of the compilation</returns>
        public CompilerResults Compile(CodeCompileUnit code)
        {
            CompilerParameters options = new CompilerParameters();
            options.GenerateExecutable = false;
            Type langType = typeof(T);
            options.ReferencedAssemblies.Add(langType.Assembly.Location); //adding the reference of the language
            //adding all the referenced assemblies the laguage uses
            //TODO determine how to recursively acquire the assemblies of the referenced assemblies
            foreach (AssemblyName assembly in langType.Assembly.GetReferencedAssemblies())
            {
                if (!assembly.Name.Contains("mscorlib"))
                {
                    options.ReferencedAssemblies.Add(assembly.Name + ".dll");
                }
            }
            options.OutputAssembly = code.Namespaces[0].Name + ".dll"; //nameing the dll
            return provider.CompileAssemblyFromDom(options, code); //compiling and returning the results
        }

        /// <summary>
        /// Generates a local variable name and type declaration statement
        /// </summary>
        /// <param name="id">The integer id of the var</param>
        /// <param name="type">The type of the var (default is var)</param>
        /// <returns></returns>
        private static CodeVariableDeclarationStatement MakeVariable(int id, string type = "var")
        {
            return new CodeVariableDeclarationStatement(type, String.Format("var{0:0000}",id));
        }

        /// <summary>
        /// The Inline build method for defining a method
        /// </summary>
        /// <param name="method">the method prepared for statements</param>
        /// <param name="program">the program to define in method</param>
        private static void InlineBuild(ref CodeMemberMethod method, IProgramable program)
        {
            int varcount = 0;
            CodeVariableReferenceExpression langrefexpr = new CodeVariableReferenceExpression("lang");

            Stack<Tuple<ICompilable, CodeExpressionCollection>> code = new Stack<Tuple<ICompilable, CodeExpressionCollection>>();
            code.Push(new Tuple<ICompilable, CodeExpressionCollection>(program.GetProgramRoot(), new CodeExpressionCollection()));

            Tuple<ICompilable, CodeExpressionCollection> current = null;
            while (code.Count > 0)
            {
                current = code.Pop();
                if (current.Item1.HasArguments())
                {
                    ICompilable[] args = current.Item1.GetArguments();
                    int i;
                    for (i = current.Item2.Count; i < args.Length; i++)
                    {
                        if (args[i].HasArguments())
                        {
                            code.Push(new Tuple<ICompilable, CodeExpressionCollection>(current.Item1, current.Item2));
                            code.Push(new Tuple<ICompilable, CodeExpressionCollection>(args[i], new CodeExpressionCollection()));
                            break;
                        }
                        else
                        {
                            CodeExpression expr = args[i].ToCodeExpression();
                            if (expr is CodeParameterDeclarationExpression)
                            {
                                CodeParameterDeclarationExpression paramexpr = expr as CodeParameterDeclarationExpression;
                                CodeVariableReferenceExpression refexpr = new CodeVariableReferenceExpression(paramexpr.Name);
                                current.Item2.Add(refexpr);
                                bool add = true;
                                foreach (CodeParameterDeclarationExpression param in method.Parameters)
                                {
                                    if (param.Name == paramexpr.Name)
                                    {
                                        add = false;
                                        break;
                                    }
                                }
                                if (add)
                                {
                                    method.Parameters.Add(paramexpr);
                                }
                            }
                            else if (expr is CodePrimitiveExpression)
                            {
                                current.Item2.Add(expr);
                            }
                        }
                    }
                    if (i < args.Length) continue; //this means that a function was just pushed to the stack
                    else //All arguments have been processed.
                    {
                        //putting the function invocation into the next function
                        CodeMethodReferenceExpression refexpr = (CodeMethodReferenceExpression)current.Item1.ToCodeExpression();
                        refexpr.TargetObject = langrefexpr;
                        CodeMethodInvokeExpression methodexpr = new CodeMethodInvokeExpression();
                        methodexpr.Method = refexpr;
                        methodexpr.Parameters.AddRange(current.Item2);
                        if (code.Count <= 0) break;
                        else //writing the method into a variable
                        {
                            CodeVariableDeclarationStatement decstate = new CodeVariableDeclarationStatement(((INode)current.Item1).ValueType, "var" + varcount.ToString(), methodexpr);
                            method.Statements.Add(decstate);
                            varcount++;
                            CodeVariableReferenceExpression varrefexpr = new CodeVariableReferenceExpression(decstate.Name);
                            code.Peek().Item2.Add(varrefexpr);
                        }
                    }
                }
            }

            CodeExpression finalexpr = current.Item1.ToCodeExpression();
            if (finalexpr is CodeMethodReferenceExpression)
            {
                CodeMethodReferenceExpression refexpr = finalexpr as CodeMethodReferenceExpression;
                refexpr.TargetObject = langrefexpr;
                CodeMethodInvokeExpression methodexpr = new CodeMethodInvokeExpression();
                methodexpr.Method = refexpr;
                methodexpr.Parameters.AddRange(current.Item2);
                finalexpr = methodexpr;
            }
            else if (finalexpr is CodeParameterDeclarationExpression)
            {
                CodeParameterDeclarationExpression paramexpr = finalexpr as CodeParameterDeclarationExpression;
                CodeVariableReferenceExpression refexpr = new CodeVariableReferenceExpression(paramexpr.Name);
                finalexpr = refexpr;
                bool add = true;
                foreach (CodeParameterDeclarationExpression param in method.Parameters)
                {
                    if (param.Name == paramexpr.Name)
                    {
                        add = false;
                        break;
                    }
                }
                if (add)
                {
                    method.Parameters.Add(paramexpr);
                }
            }
            else if (finalexpr is CodePrimitiveExpression)
            {
                //nothing to do
            }

            CodeMethodReturnStatement ret = new CodeMethodReturnStatement(finalexpr);
            method.Statements.Add(ret);
        }

        /// <summary>
        /// The nested method call approach to building the method
        /// </summary>
        /// <param name="method">the method to be constructed</param>
        /// <param name="program">the program to be represented in method</param>
        private static void NestedBuild(ref CodeMemberMethod method, IProgramable program)
        {
            CodeVariableReferenceExpression langrefexpr = new CodeVariableReferenceExpression("lang");

            Stack<Tuple<ICompilable, CodeExpressionCollection>> code = new Stack<Tuple<ICompilable, CodeExpressionCollection>>();
            code.Push(new Tuple<ICompilable, CodeExpressionCollection>(program.GetProgramRoot(), new CodeExpressionCollection()));

            Tuple<ICompilable, CodeExpressionCollection> current = null;
            while (code.Count > 0)
            {
                current = code.Pop();
                if (current.Item1.HasArguments())
                {
                    ICompilable[] args = current.Item1.GetArguments();
                    int i;
                    for (i = current.Item2.Count; i < args.Length; i++)
                    {
                        if (args[i].HasArguments())
                        {
                            code.Push(new Tuple<ICompilable, CodeExpressionCollection>(current.Item1, current.Item2));
                            code.Push(new Tuple<ICompilable, CodeExpressionCollection>(args[i], new CodeExpressionCollection()));
                            break;
                        }
                        else
                        {
                            CodeExpression expr = args[i].ToCodeExpression();
                            if (expr is CodeParameterDeclarationExpression)
                            {
                                CodeParameterDeclarationExpression paramexpr = expr as CodeParameterDeclarationExpression;
                                CodeVariableReferenceExpression refexpr = new CodeVariableReferenceExpression(paramexpr.Name);
                                current.Item2.Add(refexpr);
                                bool add = true;
                                foreach (CodeParameterDeclarationExpression param in method.Parameters)
                                {
                                    if (param.Name == paramexpr.Name)
                                    {
                                        add = false;
                                        break;
                                    }
                                }
                                if (add)
                                {
                                    method.Parameters.Add(paramexpr);
                                }
                            }
                            else if (expr is CodePrimitiveExpression)
                            {
                                current.Item2.Add(expr);
                            }
                        }
                    }
                    if (i < args.Length) continue; //this means that a function was just pushed to the stack
                    else //All arguments have been processed.
                    {
                        //putting the function invocation into the next function
                        CodeMethodReferenceExpression refexpr = (CodeMethodReferenceExpression)current.Item1.ToCodeExpression();
                        refexpr.TargetObject = langrefexpr;
                        CodeMethodInvokeExpression methodexpr = new CodeMethodInvokeExpression();
                        methodexpr.Method = refexpr;
                        methodexpr.Parameters.AddRange(current.Item2);
                        if (code.Count <= 0) break;
                        else
                        {
                            code.Peek().Item2.Add(methodexpr);
                        }
                    }
                }
            }

            CodeExpression finalexpr = current.Item1.ToCodeExpression();
            if (finalexpr is CodeMethodReferenceExpression)
            {
                CodeMethodReferenceExpression refexpr = finalexpr as CodeMethodReferenceExpression;
                refexpr.TargetObject = langrefexpr;
                CodeMethodInvokeExpression methodexpr = new CodeMethodInvokeExpression();
                methodexpr.Method = refexpr;
                methodexpr.Parameters.AddRange(current.Item2);
                finalexpr = methodexpr;
            }
            else if (finalexpr is CodeParameterDeclarationExpression)
            {
                CodeParameterDeclarationExpression paramexpr = finalexpr as CodeParameterDeclarationExpression;
                CodeVariableReferenceExpression refexpr = new CodeVariableReferenceExpression(paramexpr.Name);
                finalexpr = refexpr;
                bool add = true;
                foreach (CodeParameterDeclarationExpression param in method.Parameters)
                {
                    if (param.Name == paramexpr.Name)
                    {
                        add = false;
                        break;
                    }
                }
                if (add)
                {
                    method.Parameters.Add(paramexpr);
                }
            }
            else if (finalexpr is CodePrimitiveExpression)
            {
                //nothing to do
            }

            CodeMethodReturnStatement ret = new CodeMethodReturnStatement(finalexpr);
            method.Statements.Add(ret);
        }
    }
}
