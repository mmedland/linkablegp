﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.CodeDom;

namespace LinkableGP.CodeGeneration
{
    /// <summary>
    /// This interface is intended to allow a chromosome to create a compile unit from CodeDOM
    /// </summary>
    public interface IProgramable
    {
        /// <summary>
        /// gets the root of the program tree.
        /// </summary>
        /// <returns>The program root</returns>
        ICompilable GetProgramRoot();

        /// <summary>
        /// Gets the return type of the program tree.
        /// </summary>
        /// <returns>the return type</returns>
        Type GetReturnType();
    }
}
