﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.CodeDom;

namespace LinkableGP.CodeGeneration
{
    /// <summary>
    /// Provides the Method to generate a CodeDOM Graph of the representative function or terminal
    /// </summary>
    public interface ICompilable
    {
        /// <summary>
        /// Implementations of this method should provide a code expression using its internal definition
        /// </summary>
        /// <returns>A Expression in CodeDOM format</returns>
        CodeExpression ToCodeExpression();

        /// <summary>
        /// Implementations of this method should return the compilable arguments of this instance
        /// </summary>
        /// <returns>The compilable arguments</returns>
        ICompilable[] GetArguments();

        bool HasArguments();
    }
}
