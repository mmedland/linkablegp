﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LinkableGP.Architecture;
using LinkableGP.Agents;

namespace LinkableGP.Algorithms.Operations
{
    public class GrowInitialize : IChromosomeInitialize
    {
        public GrowInitialize()
        {
        }

        public Chromosome BuildChromosome<T>(LanguagePool<T> languagePool, int maxDepth) where T : new()
        {
            Chromosome c = new Chromosome(maxDepth);
            c.Root = languagePool.GetNodeOfType();
            Queue<Tuple<Function,int>> queue = new Queue<Tuple<Function,int>>();
            if (c.Root is Function)
            {
                queue.Enqueue(new Tuple<Function, int>(c.Root as Function, 0));
                while (queue.Count > 0)
                {
                    Tuple<Function, int> current = queue.Dequeue();
                    if (current.Item2 < maxDepth - 1)
                    {
                        for (int i = 0; i < current.Item1.Arguments.Length; i++)
                        {
                            current.Item1.Arguments[i] = languagePool.GetNodeOfType(current.Item1.ArgumentTypes[i]);
                            if (current.Item1.Arguments[i] is Function)
                            {
                                queue.Enqueue(new Tuple<Function, int>(current.Item1.Arguments[i] as Function, current.Item2 + 1));
                            }
                        }
                    }
                    else
                    {
                        for (int i = 0; i < current.Item1.Arguments.Length; i++)
                        {
                            current.Item1.Arguments[i] = languagePool.GetTerminalOfType(current.Item1.ArgumentTypes[i]);
                        }
                    }
                }
            }
            return c;
        }

        public object Clone()
        {
            return new GrowInitialize();
        }
    }
}
