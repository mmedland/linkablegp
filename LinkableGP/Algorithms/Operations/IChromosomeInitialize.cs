﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LinkableGP.Agents;
using LinkableGP.Architecture;

namespace LinkableGP.Algorithms.Operations
{
    public interface IChromosomeInitialize: ICloneable
    {
        Chromosome BuildChromosome<T>(LanguagePool<T> languagePool, int maxDepth) where T : new();
    }
}
