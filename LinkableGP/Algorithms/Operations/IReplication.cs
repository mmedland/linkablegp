﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LinkableGP.Agents;

namespace LinkableGP.Algorithms.Operations
{
    /// <summary>
    /// This interface is for replication. While replication and cloning have the same
    /// properties, this operation is here to allow for the possibility of further alteration
    /// </summary>
    public interface IReplication : ICloneable
    {
        /// <summary>
        /// Implementations should make an exact copy of the tree
        /// </summary>
        /// <param name="original">the original</param>
        /// <param name="copy">the copy</param>
        bool Replicate(Chromosome original, out Chromosome copy);
    }
}
