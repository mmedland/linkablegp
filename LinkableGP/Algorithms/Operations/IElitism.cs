﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LinkableGP.Agents;

namespace LinkableGP.Algorithms.Operations
{
    public interface IElitism : ICloneable
    {
        Chromosome[] SelectFrom(Population pop);
    }
}
