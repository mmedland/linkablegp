﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LinkableGP.Architecture;
using LinkableGP.Agents;

namespace LinkableGP.Algorithms.Operations
{
    /// <summary>
    /// This Mutation operation performs a basic mutation.
    /// A point is selected within the program tree (increasing probability toward the
    /// bottom of the tree) and replaces it with a terminal node.
    /// </summary>
    public class BasicMutation : IMutation
    {
        /// <summary>
        /// Constructs a new basic mutation operation
        /// </summary>
        public BasicMutation()
        {
        }

        
        public bool Mutate<T>(LanguagePool<T> languagePool, Random random, Chromosome mutee, out Chromosome mutant) where T : new()
        {
            mutant = (Chromosome)mutee.Clone();

            if (mutant.Root is Function)
            {
                Function parent = mutant.Root as Function;
                int sel = random.Next(parent.Arguments.Length);
                INode child = parent.Arguments[sel];
                while (true)
                {
                    if (child is Terminal || random.NextDouble() < 1 - (child.Height /(double) mutant.Root.Height)) 
                    {
                        parent.SetArgument(sel, languagePool.GetTerminalOfType(parent.ArgumentTypes[sel]));
                        break;
                    }
                    else //the child is a function
                    {
                        parent = child as Function;
                        sel = random.Next(parent.Arguments.Length);
                        child = parent.Arguments[sel];
                    }
                }
            }
            else
            {
                mutant.Root = languagePool.GetTerminalOfType(mutant.Root.ValueType);
            }
            return true;
        }

        /// <summary>
        /// Clones the mutation operation
        /// </summary>
        /// <returns>A copy of the mutation operation</returns>
        public object Clone()
        {
            return new BasicMutation();
        }
    }
}
