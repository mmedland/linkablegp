﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LinkableGP.Agents;

namespace LinkableGP.Algorithms.Operations
{
    /// <summary>
    /// Tournament selection take k random competitors and returns the most fit competitor.
    /// </summary>
    public class TournamentSelection : ISelection
    {
        /// <summary>
        /// Number of competitors
        /// </summary>
        int _k;

        /// <summary>
        /// Constructs an instance of a tournament selection operation.
        /// </summary>
        /// <param name="k"></param>
        public TournamentSelection(int k = 3)
        {
            _k = k;
        }

        /// <summary>
        /// Selects the best of k random chromosome using the IFitness type of the chromosome
        /// </summary>
        /// <param name="pop">the population</param>
        /// <param name="selected">the selected chromosome</param>
        public bool Select(Population pop, out Chromosome selected)
        {
            selected = pop.GetRandom();
            for (int i = 1; i < _k; i++)
            {
                Chromosome competitor = pop.GetRandom();
                if (competitor > selected)
                {
                    selected = competitor;
                }
            }
            return true;
        }

        /// <summary>
        /// Creates an identical instance of this selection operation.
        /// </summary>
        /// <returns>A copy of this tournament selection</returns>
        public object Clone()
        {
            return new TournamentSelection(_k);
        }
    }
}
