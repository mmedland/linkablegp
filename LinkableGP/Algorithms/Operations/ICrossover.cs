﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LinkableGP.Agents;
using LinkableGP.Architecture;

namespace LinkableGP.Algorithms.Operations
{
    /// <summary>
    /// Interface Specifying the Crossover operation
    /// </summary>
    public interface ICrossover : ICloneable
    {
        /// <summary>
        /// Implementations of this method should return two new chromosomes based on the given chromosomes
        /// </summary>
        /// <param name="p1">parent 1</param>
        /// <param name="p2">parent 2</param>
        /// <param name="c1">child 1</param>
        /// <param name="c2">child 2</param>
        bool Crossover(Random random, Chromosome p1, Chromosome p2, out Chromosome c1, out Chromosome c2);
    }
}
