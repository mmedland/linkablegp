﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LinkableGP.Algorithms.Operations
{
    public class RouletteSelection :ISelection
    {
        public RouletteSelection() { }

        public bool Select(Agents.Population pop, out Agents.Chromosome selected)
        {
            Random rand = new Random();
            double[] props = new double[pop.Size];
            props[0] = pop[0].Fitness.FitnessProportion;
            for (int i = 1; i < pop.Size; i++)
                props[i] = props[i - 1] + pop[i].Fitness.FitnessProportion;
            double sel = rand.NextDouble();
            selected = pop[Array.FindIndex(props,x=>x>=sel)];
            return true;
        }

        public object Clone()
        {
            return new RouletteSelection();
        }
    }
}
