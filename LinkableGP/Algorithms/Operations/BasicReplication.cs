﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LinkableGP.Agents;

namespace LinkableGP.Algorithms.Operations
{
    /// <summary>
    /// Simple replication that does nothing more than clone the chromosome
    /// </summary>
    public class BasicReplication : IReplication
    {
        /// <summary>
        /// New instance of the operators
        /// </summary>
        public BasicReplication()
        {
        }

        /// <summary>
        /// Clones the chromosome
        /// </summary>
        /// <param name="original">the original</param>
        /// <param name="copy">the copy</param>
        public bool Replicate(Chromosome original, out Chromosome copy)
        {
            copy = (Chromosome)original.Clone();
            return true;
        }

        /// <summary>
        /// Clones the operator
        /// </summary>
        /// <returns>A copy of the basic replicator</returns>
        public object Clone()
        {
            return new BasicReplication();
        }
    }
}
