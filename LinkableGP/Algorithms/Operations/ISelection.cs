﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LinkableGP.Agents;

namespace LinkableGP.Algorithms.Operations
{
    /// <summary>
    /// This interface represents the selection algorithm for the genetic algorithm
    /// </summary>
    public interface ISelection : ICloneable
    {
        /// <summary>
        /// Implementation should return a member of the population.
        /// </summary>
        /// <param name="pop">the population</param>
        /// <param name="selected">the selected member of the population.</param>
        bool Select(Population pop, out Chromosome selected);
    }
}
