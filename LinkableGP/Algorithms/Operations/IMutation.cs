﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LinkableGP.Agents;
using LinkableGP.Architecture;

namespace LinkableGP.Algorithms.Operations
{
    /// <summary>
    /// Interface specifying the method for mutation
    /// </summary>
    public interface IMutation : ICloneable
    {
        /// <summary>
        /// Implementations of this method should return a mutation of the original chromosome
        /// </summary>
        /// <param name="mutatee">the original chromosome</param>
        /// <param name="mutant">the mutated chromosome</param>
        bool Mutate<T>(LanguagePool<T> languagePool, Random random, Chromosome mutee, out Chromosome mutant) where T : new();
    }
}
