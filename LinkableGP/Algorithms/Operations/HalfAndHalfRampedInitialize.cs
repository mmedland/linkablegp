﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LinkableGP.Agents;
using LinkableGP.Architecture;

namespace LinkableGP.Algorithms.Operations
{
    public class HalfAndHalfRampedInitialize : IChromosomeInitialize
    {
        private GrowInitialize grow;
        private FullInitialize full;
        private Random rand;
        private double growratio;

        public HalfAndHalfRampedInitialize(double growratio = 0.5)
        {
            this.growratio = growratio;
            rand = new Random();
            grow = new GrowInitialize();
            full = new FullInitialize();
        }


        public Chromosome BuildChromosome<T>(LanguagePool<T> languagePool, int maxDepth) where T : new()
        {
            return rand.NextDouble() < 0.5 ? 
                grow.BuildChromosome(languagePool, maxDepth) : full.BuildChromosome(languagePool, maxDepth);
        }

        public object Clone()
        {
            return new HalfAndHalfRampedInitialize(growratio);
        }
    }
}
