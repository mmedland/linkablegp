﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LinkableGP.Agents;

namespace LinkableGP.Algorithms.Operations
{
    public class BasicElitism : IElitism
    {
        double prop;
        int count;

        private BasicElitism()
        {
        }

        public BasicElitism(double prop)
        {
            this.prop = prop;
            count = 0;
        }

        public BasicElitism(int count)
        {
            this.count = count;
            prop = 0;
        }

        public Chromosome[] SelectFrom(Agents.Population pop)
        {
            int size;
            Chromosome[] elite;
            if (count > 0)
            {
                size = count;
                elite = new Chromosome[count];
            }
            else if (prop > 0)
            {
                size = (int)Math.Ceiling(prop * pop.Size);
                elite = new Chromosome[size];
            }
            else
            {
                elite = new Chromosome[0];
                return elite;
            }
            List<Chromosome> p = pop.ToList();
            p.Sort();
            for (int i = 0; i < size; i++)
            {
                elite[i] = p[p.Count - i - 1];
            }
            return elite;
        }

        public object Clone()
        {
            return new BasicElitism() { prop = this.prop, count = this.count };
        }
    }
}
