﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LinkableGP.Architecture;
using LinkableGP.Agents;

namespace LinkableGP.Algorithms.Operations
{
    /// <summary>
    /// This is a basic crossover implementation.
    /// Two matching return types are selected, one from p1 and one from p2,
    /// then the two points are swaped with their subtrees.
    /// </summary>
    public class BasicCrossover : ICrossover
    {
        public BasicCrossover()
        {
        }

        /// <summary>
        /// Crossover operation.
        /// </summary>
        /// <param name="random">a random generator</param>
        /// <param name="p1">parent 1</param>
        /// <param name="p2">parent 2</param>
        /// <param name="c1">child 1</param>
        /// <param name="c2">child 2</param>
        public bool Crossover(Random random, Chromosome p1, Chromosome p2, out Chromosome c1, out Chromosome c2)
        {
            c1 = (Chromosome)p1.Clone();
            c2 = (Chromosome)p2.Clone();
            List<Tuple<Function, INode, int, int>> choices = new List<Tuple<Function, INode, int, int>>();
            choices.Add(new Tuple<Function, INode, int, int>(null, c1.Root, 0, 0));

            Queue<Tuple<INode, int>> queue = new Queue<Tuple<INode, int>>();
            queue.Enqueue(new Tuple<INode, int>(c1.Root, 0));
            while (queue.Count > 0)
            {
                Tuple<INode, int> current = queue.Dequeue();
                if (current.Item1 is Function)
                {
                    Function node = current.Item1 as Function;
                    for (int i = 0; i < node.Arguments.Length; i++)
                    {
                        queue.Enqueue(new Tuple<INode, int>(node.Arguments[i], current.Item2 + 1));
                        choices.Add(new Tuple<Function, INode, int, int>(node, node.Arguments[i], current.Item2 + 1, i));
                    }
                }
            }
            if (c1.Root.Height > 0)
            {
                for (int i = choices.Count - 1; i >= 0; i--)
                {
                    if ((choices[i].Item3 + 1)/ c1.Root.Height < random.NextDouble())
                    {
                        choices.RemoveAt(i);
                    }
                }
            }

            Tuple<Function, INode, int, int> c1Choice = choices[random.Next(choices.Count)];

            choices.Clear();
            queue.Clear();
            queue.Enqueue(new Tuple<INode, int>(c2.Root, 0));
            while (queue.Count > 0)
            {
                Tuple<INode, int> current = queue.Dequeue();
                if (current.Item1 is Function)
                {
                    Function node = current.Item1 as Function;
                    for (int i = 0; i < node.Arguments.Length; i++)
                    {
                        queue.Enqueue(new Tuple<INode, int>(node.Arguments[i], current.Item2 + 1));
                        if (node.ArgumentTypes[i] == c1Choice.Item2.ValueType)
                        {
                            choices.Add(new Tuple<Function, INode, int, int>(node, node.Arguments[i], current.Item2 + 1, i));
                        }
                    }
                }
            }

            if (c2.Root.Height>0)
            {
                for (int i = choices.Count - 1; i >= 0; i--)
                {
                    if (choices[i].Item3 + c1Choice.Item2.Height > p1.MaxDepth || choices[i].Item2.Height + c1Choice.Item3 > p1.MaxDepth)
                    {
                        choices.RemoveAt(i);
                    }
                    else if ((choices[i].Item3 + 1) / c2.Root.Height < random.NextDouble())
                    {
                        choices.RemoveAt(i);
                    }
                } 
            }

            //choices.RemoveAll(x => x.Item3 + c1Choice.Item2.Height > p1.MaxDepth ||
            //    x.Item2.Height + c1Choice.Item3 > p1.MaxDepth);
            if (choices.Count > 0)
            {
                Tuple<Function, INode, int, int> c2Choice = choices[random.Next(choices.Count)];
                if (c1Choice.Item1 != null)
                {
                    c1Choice.Item1.Arguments[c1Choice.Item4] = c2Choice.Item2;
                }
                else
                {
                    c1.Root = c2Choice.Item2;
                }
                if (c2Choice.Item1 != null)
                {
                    c2Choice.Item1.Arguments[c2Choice.Item4] = c1Choice.Item2;
                }
                else
                {
                    c2.Root = c1Choice.Item2;
                }
                return true; //IMPORTANT
            }
            else
            {
                c1 = null;
                c2 = null;
                return false;
            }
        }

        public object Clone()
        {
            return new BasicCrossover();
        }
    }
}
