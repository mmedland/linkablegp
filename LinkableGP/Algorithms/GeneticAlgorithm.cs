﻿//#define Linear
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LinkableGP.Agents;
using System.Threading.Tasks;
using System.Threading;
using LinkableGP.Architecture;
using LinkableGP.Interfaces;

namespace LinkableGP.Algorithms
{
    class GeneticAlgorithm
    {
        private double crossrate;
        private double mutrate;
        private double elitismRate;
        private GeneticOperations ops;
        private int maxDepth;

        public GeneticAlgorithm(double crossrate, double mutrate, GeneticOperations operations, int maxDepth, double elitismRate)
        {
            this.crossrate = crossrate;
            this.mutrate = mutrate;
            this.elitismRate = elitismRate;
            this.ops = operations;
            this.maxDepth = maxDepth;
        }

        public Population Run<T, U>(Population pop, LanguagePool<T> languagePool) where T : new() where U : IEnvironment, new()
        {
            PopulationBuilder builder = new PopulationBuilder(pop.Size);
            Semaphore semaphore = new Semaphore(0, Environment.ProcessorCount);
            IEnumerable<Chromosome> best = ops.GetElite(pop);
            {
                U envir = new U();
                object langInst = languagePool.GetLanguageInstance();
                foreach (Chromosome chromosome in best)
                {
                    envir.Evaluate(chromosome, langInst);
                    chromosome.MaxDepth += chromosome.MaxDepth < maxDepth ? 1 : 0;
                    builder.Add(chromosome);
                }
            }
#if Linear
            {
                U envir = new U();
                object langInst = languagePool.GetLanguageInstance();
                Random rand = new Random();
                GeneticOperations tops;
                lock (ops)
                {
                    tops = (GeneticOperations)ops.Clone();
                }
                while (true)
                {
                    if (crossrate > rand.NextDouble())
                    {
                        Chromosome p1, p2, c1, c2;
                        ops.Select(pop, out p1);
                        ops.Select(pop, out p2);
                        if (ops.Crossover(rand, p1, p2, out c1, out c2))
                        {
                            envir.Evaluate(c1, langInst);
                            c1.MaxDepth += c1.MaxDepth < maxDepth ? 1 : 0;
                            envir.Evaluate(c2, langInst);
                            c2.MaxDepth += c2.MaxDepth < maxDepth ? 1 : 0;
                            if (!builder.TryAdd(c1)) break;
                            if (!builder.TryAdd(c2)) break;
                        }
                    }
                    else if (mutrate > rand.NextDouble())
                    {
                        Chromosome sel, mut;
                        ops.Select(pop, out sel);
                        lock (languagePool)
                        {
                            ops.Mutate(languagePool, rand, sel, out mut);
                        }
                        envir.Evaluate(mut, langInst);
                        mut.MaxDepth += mut.MaxDepth < maxDepth ? 1 : 0;
                        if (!builder.TryAdd(mut)) break;
                    }
                    else
                    {
                        Chromosome sel, clone;
                        ops.Select(pop, out sel);
                        ops.Replicate(sel, out clone);
                        envir.Evaluate(clone, langInst);
                        clone.MaxDepth += clone.MaxDepth < maxDepth ? 1 : 0;
                        if (!builder.TryAdd(clone)) break;
                    }
                }
            }
#else
            Random rand = new Random();
            int max = pop.Size - builder.Count;

            max = (int)Math.Ceiling(max / (double)(Environment.ProcessorCount));
            for (int i = 0; i < Environment.ProcessorCount; i++)
            {
                new Thread(new ThreadStart(() =>
                    {
                        int added = 0;
                        U envir = new U();
                        object langInst = languagePool.GetLanguageInstance();
                        while (added < max)
                        {
                            double r1, r2;
                            lock (rand)
                            {
                                r1 = rand.NextDouble();
                                r2 = rand.NextDouble();
                            }
                            if (crossrate > r1)
                            {
                                Chromosome p1, p2, c1, c2;
                                ops.Select(pop, out p1);
                                ops.Select(pop, out p2);
                                if (ops.Crossover(rand, p1, p2, out c1, out c2))
                                {
                                    envir.Evaluate(c1, langInst);
                                    c1.MaxDepth += c1.MaxDepth < maxDepth ? 1 : 0;
                                    envir.Evaluate(c2, langInst);
                                    c2.MaxDepth += c2.MaxDepth < maxDepth ? 1 : 0;
                                    if (!builder.TryAdd(c1)) break;
                                    if (!builder.TryAdd(c2)) break;
                                    added += 2;
                                }
                            }
                            else if (mutrate > r2)
                            {
                                Chromosome sel, mut;
                                ops.Select(pop, out sel);
                                lock (languagePool)
                                {
                                    ops.Mutate(languagePool, rand, sel, out mut);
                                }
                                envir.Evaluate(mut, langInst);
                                mut.MaxDepth += mut.MaxDepth < maxDepth ? 1 : 0;
                                if (!builder.TryAdd(mut)) break;
                                added ++;
                            }
                            else
                            {
                                Chromosome sel, clone;
                                ops.Select(pop, out sel);
                                ops.Replicate(sel, out clone);
                                envir.Evaluate(clone, langInst);
                                clone.MaxDepth += clone.MaxDepth < maxDepth ? 1 : 0;
                                if (!builder.TryAdd(clone)) break;
                                added++;
                            }
                        }
                        semaphore.Release(1);
                    }
                )).Start();
            }

            for (int i = 0; i < Environment.ProcessorCount; i++)
            {
                semaphore.WaitOne();
            }
#endif
            return builder.ToPopulation();
        }
    }
}
