﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LinkableGP.Algorithms.Operations;
using LinkableGP.Agents;
using LinkableGP.Architecture;

namespace LinkableGP.Algorithms
{
    public class GeneticOperations : ICloneable
    {

        private ICrossover crossOp;
        private IMutation mutOp;
        private IReplication repOp;
        private ISelection selOp;
        private IChromosomeInitialize chromInitOp;
        private IElitism elitismOp;


        /// <summary>
        /// Creates an instance of Genetic Operations using
        /// BasicCrossover, BasicMutation, BasicReplication and
        /// TournamentSelection (k=3).
        /// </summary>
        public GeneticOperations()
        {
            crossOp = new BasicCrossover();
            mutOp = new BasicMutation();
            repOp = new BasicReplication();
            selOp = new TournamentSelection();
            chromInitOp = new HalfAndHalfRampedInitialize();
            elitismOp = new BasicElitism(1);
        }

        /// <summary>
        /// Creates an instance of Genetic Operations using the given operations
        /// </summary>
        /// <param name="crossover">The crossover operation</param>
        /// <param name="mutation">The mutation operation</param>
        /// <param name="replication">The replication operation</param>
        /// <param name="selection">The selection operation</param>
        public GeneticOperations(ICrossover crossover, IMutation mutation, IReplication replication, ISelection selection, IChromosomeInitialize chromInit, IElitism elitism)
        {
            crossOp = crossover;
            mutOp = mutation;
            repOp = replication;
            selOp = selection;
            chromInitOp = chromInit;
            elitismOp = elitism;
        }

        internal bool Crossover(Random random, Chromosome p1, Chromosome p2, out Chromosome c1, out Chromosome c2)
        {
            return crossOp.Crossover(random, p1, p2, out c1, out c2);
        }

        internal bool Mutate<T>(LanguagePool<T> languagePool, Random random, Chromosome mutee, out Chromosome mutant) where T : new()
        {
            return mutOp.Mutate(languagePool, random, mutee, out mutant);
        }

        internal bool Replicate(Chromosome original, out Chromosome copy)
        {
            return repOp.Replicate(original, out copy);
        }

        internal bool Select(Population pop, out Chromosome selected)
        {
            return selOp.Select(pop, out selected);
        }

        internal Chromosome[] GetElite(Population pop)
        {
            return elitismOp.SelectFrom(pop);
        }

        internal Chromosome BuildChromosome<T>(LanguagePool<T> languagePool, int maxDepth) where T : new()
        {
            return chromInitOp.BuildChromosome(languagePool, maxDepth);
        }

        public object Clone()
        {
            return new GeneticOperations((ICrossover)crossOp.Clone(), (IMutation)mutOp.Clone(), 
                (IReplication)repOp.Clone(), (ISelection)selOp.Clone(), (IChromosomeInitialize)chromInitOp.Clone(),(IElitism)elitismOp.Clone());
        }
    }
}
